#!/usr/bin/env python

import os
import sys
from shutil import copyfile, copy
from time import sleep


"""
This script generates a compressed filesystem, a squashfs, and copies it and initrd as well as vmlinuz over to
the "./build" folder.
"""


class colors:
       HEADER = '\033[95m'
       OKBLUE = '\033[94m'
       OKGREEN = '\033[92m'
       WARNING = '\033[93m'
       FAIL = '\033[91m'
       ENDC = '\033[0m'
       BOLD = '\033[1m'
       UNDERLINE = '\033[4m'

splash = """
   _____                _____        __ _   
  / ____|              / ____|      / _| |  
 | |     ___  _ __ ___| (___   ___ | |_| |_ 
 | |    / _ \| '__/ _ \\___ \ / _ \|  _| __|
 | |___| (_) | | |  __/____) | (_) | | | |_ 
  \_____\___/|_|  \___|_____/ \___/|_|  \__|
                                           
"""


croot_exists = False

scripts = ['scripts/discovery.py', 'scripts/findwin.py', 'scripts/prepare.py', 'scripts/main.py','scripts/common.py']
misc_files = ['']
folder_list = ['']

# Clears console on Linux. 
os.system('clear')
print(colors.BOLD + splash + colors.ENDC)
sleep(1)


# Check if files exist...


# Check if chroot folder exists
print ('Checking for chroot folder...')
if (os.path.isdir("chroot") == False):
   print(colors.FAIL + 'Chroot folder does not exist. Aborting' + colors.ENDC)
   sleep(1)
   sys.exit()
   
elif (os.path.isdir("chroot") == True):
   print(colors.OKGREEN + 'Chroot folder exists. Continuing...' + colors.ENDC)
   chroot_present = True
else:
   pass


# If config.ini exists in scripts folder; delete it.
def checkForConfig():
    if (os.path.isfile('scripts/config.ini') == True):
        print(common.colors.WARNING + 'Configuration file already exists. Deleting...' + common.colors.ENDC)
        os.system('rm scripts/config.ini')
    else:
        pass


"""
# Create required folders
print('Creating folders...')
for i in folder_list:
       if (os.path.isdir(i) == True):
              pass
       elif (os.path.isdir(i) == False):
              os.makedirs(i)
"""


# Check if all files are in scripts folder
for i in scripts:
   os.path.isfile(i)
   print('{} exists'.format(i))

# Copy scripts to /root folder in chroot
print('Copying scripts over to chroot image...')
for i in scripts:
   copy(i,'chroot/root/scripts/') 



# Build squashfs
print ('Check passed')
print ('Deleting old filesystem.squashfs if it exists...')
if (os.path.isfile('build/filesystem.squashfs') == True):
    os.system('rm build/filesystem.squashfs')
    print(colors.OKGREEN + 'Deleted' + colors.ENDC)
else:
    pass
print('\nGenerating new {}filesystem.squashfs{}, please be patient.'.format(colors.OKGREEN,colors.ENDC))
os.system('sudo mksquashfs chroot build/filesystem.squashfs -e boot')
print (colors.OKGREEN +'Generated filesystem.squashfs.' + colors.ENDC)


# Copy initrd and vmlinuz to "build" folder
print ('Now copying initrd and vmlinuz...')
copyfile('chroot/boot/initrd.img-4.9.0-7-amd64', 'build/initrd')
copyfile('chroot/boot/vmlinuz-4.9.0-7-amd64', 'build/vmlinuz')

# Create ISO file


print(colors.OKGREEN + 'DONE!' + colors.ENDC)
sys.exit()


