![Logo](CoreCopy-Logo.png)

CoreCopy is a *great and easy way to quickly copy* the information from a Windows user profile onto a new computer. 

Final Study Project, Network & IT-Security. 



# Insert a screenshot here later...

## Mockup of what a future Windows client may look like. 
![Windows-mockup screenshot](screenshots/windows-mockup.png)
