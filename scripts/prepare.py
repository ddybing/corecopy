#!/usr/env/bin python

"""

This file will do some preparations as well as grab information before the main copying process begins.

This information includes the host role (sender or receiver), if the computer has an IP address and read other arguments from /proc/cmdline into the local config.ini file.

"""

import findwin
import configparser
import os
import common


config = configparser.ConfigParser()

# Over to the good stuff...


def grabcmdLine():
    F = open('cmdline')
    cmdline = F.read()
    hostrole = ''
    # Grab host role
    if 'sender' in cmdline:
        print('This host is a ' + colors.OKGREEN + 'SENDER' + colors.ENDC)

    elif 'receiver' in cmdline:
        print('This host is a ' + colors.OKGREEN + 'RECEIVER' + COLORS.ENDC)

    else:
        print ('Was not able to determine host role status (sender/receiver).\n Please enter manually.')
        hostrole = str(input('Host role: '))

    # Grab magic
    magic = '1281218'
    hostrole = 'sender'
    currentIP = '10.0.0.56'
    
    #magic = str(eval(dict(x.split('=') for x in cmdline.split())['magic']))


    # Write to config

    config.read('cc_config.ini')
    config.add_section('corecopy')
    config.set('corecopy', 'role', hostrole)
    config.set('corecopy', 'magic', magic)
    config.set('corecopy', 'ip', currentIP)

    with open('config.ini', 'w') as cc_config:
        config.write(cc_config)
    
    
    

def IPhandler():
    CurrentIP = os.system('hostname -I')
    print('My IP is {}'.format(CurrentIP))
    if (CurrentIP.startswith('169') == True):
        print ('Looks like there is no DHCP-server present. Current IP is {}. Will announce to clients using this address.'.format(CurrentIP))
        

    
    elif (CurrentIP.startswith('127') == True):
        print ('No DHCP received and no APIPA address... will set a random address in 192.168.0.0/24 network')
    elif (CurrentIP.startswith('0') == True):
        pass

    else:
        print ('IP {} looks good - proceeding...'.format(CurrentIP))


def findWinpart():
    pass

def linuxpart():
    pass

def getFileList():
    pass
