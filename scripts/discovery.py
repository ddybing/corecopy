#!/usr/env/bin python


""" 
This file will announce to and discover other clients on the network. 

It does this by binding its network address and a port
based on what role it has (receiver/sender)

"""
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST, gethostbyname, gethostname
from time import sleep
import configparser
import os
import subprocess
import ipaddress

PORT = 60000
MAGIC = "corecopy"

raw_ip = subprocess.getoutput('hostname -I')
#my_ip = raw_ip.strip()
my_ip = '10.0.0.51'
print ('My IP is {}'.format(my_ip))
s = socket(AF_INET, SOCK_DGRAM) 



def announce():
    while True:
        s.bind((my_ip, PORT))
        s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        data = MAGIC+my_ip
        s.sendto(data.encode(), ('<broadcast>', PORT))
        print ("Sent an announcement...")
        sleep(1)


def discover():
    remote_ip = ''
    while True:
        s.bind(('', PORT))
        data, addr = s.recvfrom(1024)
        print (data)
        data = data.decode('utf-8')
        if data.startswith(MAGIC):
            print ('Received packet from other client'), data[len(MAGIC):]
            remote_ip = data[len(MAGIC):]
            break
    print ('Client with magic \'{}\' found, hold on while we get connected...'.format(MAGIC))
    print ('Remote IP is {}.'.format(remote_ip))
    return remote_ip



discover()
"""
if (hoststatus == 'receiver'):
    discover()

elif (hostatus == 'sender'):
    announce()

else:
    print ("Sorry, something went wrong. Was not able to determine host status (sender/receiver)")

"""
