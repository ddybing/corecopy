#!/usr/env/python3

"""
This script will find the Windows partition on the system.

"""

import itertools
from time import sleep
import sys
import os
import shutil
import common
import threading
import re
import subprocess

winpath = '/media/ccmount/Windows'
userspath ='/media/ccmount/Users'
mountpath = '/media/ccmount'


"""
def animate():
    for progress in itertools.cycle(['|', '/', '-', '\\' ]):      
        sys.stdout.write('\rSearching for Windows drive... ' + progress)
        sys.stdout.flush()
        sleep(0.1)
"""

def finduserwin():
    # This function will check if a user folder/Windows folder exists within the the mount point.
    if os.path.isdir(winpath):
        print ('\nFound partition containing Windows, searching for Users folder...')
        if os.path.isdir(userspath):
            print('\nPartition contains Users folder...')
            return True
        else:
            return False
    else:
        pass

# This function finds the actual user that the data will be transferred from
def finduser(userpath):
    exclude_list = ['Default', 'Administrator', 'Public', 'desktop.ini', 'All Users']
    userdir = os.listdir(userspath)
    for user in userdir:
        if (user in exclude_list):
            pass
        else:
            print ('Found a potential user - {}, checking...'.format(user))
            if ('ntuser.ini' in os.listdir(userspath + '/' + user)):
                print ('Confirmed user {}. Returning user path to main function...'.format(user))
                userpath = user
                return userpath
            else:
                print('User could not be confirmed, trying the next one...')
                pass    
    

def partfind():       
    # First, get a list of all partitions

    raw_partitions = subprocess.getoutput('/sbin/blkid -o device').split()
    partitions_list = list()
    for partition in raw_partitions:
        # Get only SD (SCSI Disk(fixed disk)) block devices, e.g. /dev/sda, /dev/sdb, /dev/sdc etc... 
        if '/dev/sd' in partition:
            partitions_list.append(partition)
                        
        # If it's not SD, just pass
        else:
            pass

    return partitions_list

def findntfs(partitions_list):
    # Check what partitions are NTFS, or BitLocker-encrypted NTFS, drives. 
    ntfs_parts = dict()
    for part in partitions_list:
        disk_hex = subprocess.getoutput('xxd -l 1000 {}'.format(part))
        if 'NTFS' in disk_hex:
            print('\rFound NTFS partition {}...'.format(part))
            ntfs_parts[part] = 'NTFS'
        elif 'FVE' in disk_hex:
            print('Found BitLocker-encrypted NTFS partition {}...'.format(part))
            ntfs_parts[part] = 'B-NTFS'
    return ntfs_parts

def partmount(partitions):
    # Mount partitions one after one until a partition with Windows is found.
    # Check if the file system type is NTFS or "B-NTFS"(BitLocker encrypted NTFS volume)
    for part in partitions:
        if partitions[part] == 'NTFS':
            os.system('mount -o ro {} /media/ccmount > /dev/null'.format(part))
        elif partitions[part] == 'B-NTFS':
            print('BitLocker-encrypted drive detected, please enter password.\nIf you do not know the password, or you want to mount manually, please press CTRL+Z and rerun /root/scripts/main.py once the partition has been mounted at /media/ccmount.')
            password = input('Please enter BitLocker-password: ')
            os.system('dislocker -V {} --user-password={} -- /media/tmp'.format(part, password))
            os.system('mount -o loop /media/tmp/dislocker-file /media/ccmount > /dev/null')
        else:
            break
        result = finduserwin()
        if (result == True):
            break
        elif (result == False):
            print ('User folder and Windows not found, unmounting and trying again')
            os.system('umount /media/ccmount')
            break
            

        
## Threads
#animate_thread = threading.Thread(target = animate, args=())



def worker():
    #animate_thread.start()
    partitions = partfind()
    ntfsparts = findntfs(partitions)
    partmount(ntfsparts)
    print ('Mounted and ready to go, now looking for the user.')
    sleep(1)
    userpath = ''
    user = userfind(userpath)
    print ('User is {}.'.format(user))
    sleep(5)
    
    
worker()
