#!/usr/env/bin python

"""
This is the main Python program that handles all the other, underlaying scripts. 
It will keep running until the copy is finished, spawning threads of the scripts needed underway.

"""

from time import sleep
import threading
import os


# Custom scripts and modules
import common
import prepare
import copy

# Splash "image", will be shown in red color. 
splash = """
 /$$      /$$        /$$$$$$$$                  /$$      
| $$$    /$$$       |__  $$__/                 | $$      
| $$$$  /$$$$  /$$$$$$ | $$  /$$$$$$   /$$$$$$$| $$$$$$$ 
| $$ $$/$$ $$ /$$__  $$| $$ /$$__  $$ /$$_____/| $$__  $$
| $$  $$$| $$| $$  \__/| $$| $$$$$$$$| $$      | $$  \ $$
| $$\  $ | $$| $$      | $$| $$_____/| $$      | $$  | $$
| $$ \/  | $$| $$ /$$  | $$|  $$$$$$$|  $$$$$$$| $$  | $$
|__/     |__/|__/|__/  |__/ \_______/ \_______/|__/  |__/
                                                           
"""



print(common.colors.RED + splash + common.colors.ENDC)
sleep(1)

print('Please wait a minute to grab while information about the computer is gathered...')
prepare.checkForConfig()
prepare.grabcmdLine()


#find-systempart()

copy.copy(1322)
